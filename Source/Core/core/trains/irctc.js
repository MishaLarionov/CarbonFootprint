class irctcManager {
    constructor(footprintCore, settingsProvider) {
        console.log("irctc constructor");
        this.footprintCore = footprintCore;
        this.settingsProvider = settingsProvider;
        this.subtree = true;
        this.dataSource = "irctc"; //select one of the emission information sources from trainEmissions.json
        this.MODE = ["irctc"]; // Currently average data is available only. Hence using general IRCTC name..
        this.stations = {
            arrive: "",
            depart: ""
        };
        this.validator = new TrainsValidator("irctc");
        this.footprintCore.storeDataSource(this.dataSource);
        this.footprintCore.storeTrainSpeed("india");
    }

    setStyle(emission) {
        emission.style.fontSize = "small";
        emission.querySelector("svg").style.margin = "2px 5px";
        return emission;
    }

    insertInDom(emission, element) {
        element = this.validator.querySelector(".t_4", element).firstChild;
        emission = this.setStyle(emission);
        console.log(emission);
        if (element.getElementsByClassName("carbon").length === 0) {
            element.appendChild(emission);
        }
    }

    update() {
        //Make sure search result elements exist
        if (document.querySelectorAll(".train_avl_enq_box.fromAndToStn").length === 0) return;

        //Iterate through the elements
        let trainCards = document.querySelectorAll(".train_avl_enq_box.fromAndToStn");
        console.log("Number of trainCards: " + trainCards.length);
        trainCards.forEach(card => {
            console.log("Iterated through a card");
            //Make sure we haven't already added to this card
            if (card.getElementsByClassName("carbon").length !== 0) return;
            //Get the name and duration of the train
            let trainName = this.validator.querySelector(".trainName", card).textContent.trim();
            console.log("Train name: " + trainName);
            //Since the website doesn't use a unique class or element name, total duration is the third
            let trainDurationRaw = this.validator.querySelectorAll(".col-md-4.col-sm-4.col-xs-4.text-center", card)[2].textContent.trim();
            //Format the time into decimal hours
            let trainDuration = parseInt(trainDurationRaw.split(":")[0]) + parseInt(trainDurationRaw.split(":")[1]) / 60;
            console.log("Got train duration.");
            console.log("Train duration: " + trainDuration);

            //debugger;
            var usingAverageSpeed = true;
            for (trainTestString in trainSpeedData) {
                trainName.split(" ").forEach(word => {
                    let distanceBetween;
                    if (
                        this.footprintCore.fuzzySearch(word.toLowerCase(), trainTestString)
                    ) {
                        usingAverageSpeed = false;
                        distanceBetween = trainDuration * trainSpeedData[trainTestString];
                        return;
                    }
                });
                if (!usingAverageSpeed) break;
            }
            if (usingAverageSpeed) {
                distanceBetween = trainSpeedData["average"] * trainDuration;
            }
            this.insertInDom(this.footprintCore.getEmission([this.MODE]), card);

        });
    }
}

var WebsiteManager = irctcManager;
